print('UNIVERSIDAD NACIONAL DE LOJA ')
print('Ejercicio 2 del capítulo 3')
print('Autora: Kassandra Guevara')
print('kassandra.guevara@unl.edu.ec')

#Programa del salario usando try y except, de modo que elprograma sea capaz de gestionar entradas no numéricas con elegancia
# mostrando un mensaje y saliendo del programa. A continuación se muestran dos ejecuciones del programa:


print('Introduzca las horas:')
horas = float(input())

print('Introduzca la tarifa por hora:')
tarifa = float(input())

try:
    if horas <= 40:
        salario = (horas * tarifa)
    if horas >= 40:
        extras = horas - 40
        salario = (40 * tarifa) + (extras * 1.5 * tarifa)
    else:
        print('Error, introduzca un número')

except:
    print('Error, introduzca un número')
