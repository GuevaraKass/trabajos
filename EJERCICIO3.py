print('UNIVERSIDAD NACIONAL DE LOJA ')
print('Ejercicio 3 del capítulo 3')
print('Autora: Kassandra Guevara')
print('kassandra.guevara@unl.edu.ec')

# solicite una puntuación entre 0.0 y 1.0.
# Si la puntuación está fuera de ese rango,muestra un mensaje de error.
#Si la puntuación está entre 0.0 y 1.0, muestra la calificación

try:
    puntuación = float(input('Introduzca puntuación='))
    print('Introduzca puntuación')
    #VALIDACIÓN DEL RANGO DE PUNTUACIÓN
    if puntuación >= 0 and puntuación <= 1.0:
      if puntuación >= 0.9:
        print('Sobresaliente')
      elif puntuación >= 0.8:
        print('Notable')
      elif  puntuación >= 0.7:
        print('Bien')
      elif  puntuación >= 0.6:
        print('Suficiente')
      elif  puntuación < 0.6:
        print('Inficiente')
    else:
        print('Puntuación Incorrecta')
except:
    print ('Puntuación Incorrecta')


